#include "stdafx.h"

// The start of the Application
int App::start(const std::vector<std::string> &args)
{
	quit = false;
	width = 500;
	height = 500;
	//init buffers
	short** buffer1 = new short* [width];
	short** buffer2 = new short* [width];
	for(int i = 0; i < width; i++){
		buffer1[i] = new short[height];
		buffer2[i] = new short[height];
	}

	// fill buffer 1 with a green Forest
	for(int i = 0; i < width; i++){
		for(int j = 0; j < height; j++){
			buffer1[i][j] = 1; 
			buffer2[i][j] = 1; 
		}
	}

	//init burningWoods
	BurningWoods* bw = new BurningWoods();

	// Set the window description
	clan::DisplayWindowDescription desc_window;
	desc_window.set_title("Layered Window Example");
	desc_window.set_allow_resize(false);
	desc_window.set_layered(true);
	desc_window.show_caption(false);
	desc_window.set_size(clan::Size(width, height), false);// pixel h�he und breite variabel halten

	// Open the windows
	clan::DisplayWindow window(desc_window);
	clan::Slot slot_quit = window.sig_window_close().connect(this, &App::on_window_close, &window);
	clan::Slot slot_input_up = (window.get_ic().get_keyboard()).sig_key_up().connect(this, &App::on_input_up);

	clan::Canvas canvas(window);

	clan::GameTime game_time(25, 25);

	clan::Colorf colorRed = clan::Colorf(1.0f,0.0f,0.0f, 1.0f);
	clan::Colorf colorBlack = clan::Colorf(0.0f,0.0f,0.0f, 1.0f);
	clan::Colorf colorGreen = clan::Colorf(0.0f,1.0f,0.0f, 1.0f);

	bool bufferflip = false;
	short ** frontbuffer;
	// Run until someone presses escape
	while (!quit)
	{
		game_time.update();

		canvas.clear(clan::Colorf(0.0f,0.0f,0.0f, 1.0f));
		if(bufferflip){
			frontbuffer = buffer2;
			bufferflip = false;
			bw->calcProcess(buffer2, buffer1,width,height);

		}else{
			frontbuffer = buffer1;
			bufferflip = true;
			bw->calcProcess(buffer1, buffer2,width,height);
		}

		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				if(frontbuffer[i][j] == -1){
					canvas.draw_point(i, j, colorRed);
				}else if(frontbuffer[i][j] == 0){
					canvas.draw_point(i, j, colorBlack);
				}else{
					canvas.draw_point(i, j, colorGreen);
				}
			}
		}

		window.flip(1);

		// This call processes user input and other events
		clan::KeepAlive::process();
	}

	return 0;
}

void App::on_window_close(clan::DisplayWindow *window)
{
	quit = true;
}

void App::on_input_up(const clan::InputEvent &key)
{
	if(key.id == clan::keycode_escape)
	{
		quit = true;
	}
}


