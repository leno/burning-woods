#include <ctime>

class BurningWoods

{
public:
	BurningWoods();
	void calcProcess(short**, short**, int, int);
private:
	short** buffer1;
	short** buffer2;
	short checkNeighbours(short**, int, int);
	short chanceToBurn();
	short chanceToGrow();
};

