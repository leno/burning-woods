// This is the Application class (That is instantiated by the Program Class)
class App
{
public:
	int start(const std::vector<std::string> &args);

private:
	int height;
	int width;
	void on_window_close(clan::DisplayWindow *window);
	void on_input_up(const clan::InputEvent &key);

private:
	clan::Point last_mouse_pos;

	bool quit;
};