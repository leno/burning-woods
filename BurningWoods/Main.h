#pragma once

// This is the Program class that is called by Application
class Main
{
public:
	static int main(const std::vector<std::string> &args);
};
