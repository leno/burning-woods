#include "stdafx.h"


BurningWoods::BurningWoods(){
	srand(static_cast <unsigned> (time(0)));
}

// Green = 1, black = 0, Red = -1
void BurningWoods::calcProcess( short** bufferIN, short** bufferOUT, int width, int height){
	for(int i = 1; i < width - 1; i++){ // bound is always 1
		for(int j = 1; j < height -1; j++){ // bound is always 1
			if(bufferIN[i][j] == -1){
				bufferOUT[i][j] = 0;// a burning tree burns to ashes
			}else if(bufferIN[i][j] == 0){
				bufferOUT[i][j] = chanceToGrow();// ashes stays ashes, but has a chance to grow
			}else if(bufferIN[i][j] == 1){
				bufferOUT[i][j] = checkNeighbours(bufferIN, i, j); // Tree is alive and needs to check his neigbours
			}
		}
	}
}


short BurningWoods::checkNeighbours(short** bufferIN, int posX, int posY){
	if(bufferIN[posX][posY - 1] == -1 ||
	   bufferIN[posX][posY + 1] == -1 ||
	   bufferIN[posX - 1][posY] == -1 || 
	   bufferIN[posX + 1][posY] == -1 ||
	   bufferIN[posX - 1][posY - 1] == -1 ||
	   bufferIN[posX - 1][posY + 1] == -1||
	   bufferIN[posX + 1][posY - 1] == -1 ||
	   bufferIN[posX + 1][posY + 1] == -1)
	{
		return -1;
	}else{
		return chanceToBurn();
	}
}

short BurningWoods::chanceToGrow(){
    int random = rand()% 100000;
	double randD = random/10000.0;
	if(randD < 0.1){
		return 1;
	}
	else return 0;
}

short BurningWoods::chanceToBurn(){
	int random = rand()% 100000;
	double randD = random/10000.0;
	if(randD < 0.00001){
		return -1;
	}
	else return 1;
}



