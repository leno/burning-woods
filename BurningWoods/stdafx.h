// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <ClanLib/core.h>
#include <ClanLib/application.h>
#include <ClanLib/display.h>
#include <ClanLib/gl.h>
#include <ClanLib/swrender.h>
#include <ClanLib/d3d.h>

#include <cmath>

#include "Main.h"
#include "App.h"
#include "BurnigWoods.h";
